<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PetController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
docker run --name dbc -v $PWD/storage/mardb:/var/lib/mysql -p 3308:3306 -e MARIADB_ROOT_PASSWORD=dbp -d mariadb:latest
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::get('/pets', function () {
    return Pet::all();
});

Route::post('/pets', function () {
    return Pet::create([
        'nombre_mascota' => 'michu',
        'raza' => 'angora',
        'edad' => '4',
        'color' => 'gris',
        'telefono' => '3120927870',
        'ciudad' => 'medellin',
    ]); 
}); */

Route::resource('/pets',  PetController::class ); 

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
