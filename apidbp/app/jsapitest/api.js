/*
function showAll(url) {
    fetch(url)
    .then(response => response.json())
    .then(data => console.log(data))
}
showAll('http://127.0.0.1:8000/api/products');
function showOne(url, idNum) {
    fetch(url+ idNum)
    .then(response => response.json())
    .then(data => console.log(data))
}
//showOne('http://127.0.0.1:8000/api/products/', '9');

function destroy(url, idNum) {
    fetch(url + idNum, {
        method: 'DELETE',
        headers: {
            'Content-Type':'application/json'
        }
    })
    .then(response => response.json())
    .then(data => console.log(data))
}
//destroy('http://127.0.0.1:8000/api/products/', '7');

function saveOne(url) {
    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type':'application/json'
        },
        body : JSON.stringify({
            fn : 'listnewuuu'
        })
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response));
}
//saveOne('http://127.0.0.1:8000/api/products');

function update(url, idNum) {
    fetch(url + idNum, {
        method: 'PUT',
        headers: {
            'Content-Type':'application/json'
        },
        body : JSON.stringify({
            fn : '5555555'
        })
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response));
}
//update('http://127.0.0.1:8000/api/products/' + '27');
*/
function showAll(url) {
    fetch(url)
    .then(response => response.json())
    .then(data => console.log(data))
}
//showAll('http://127.0.0.1:8002/api/pets');
function showOne(url, idNum) {
    fetch(url+ idNum)
    .then(response => response.json())
    .catch(error => console.error('Error:', `Pet ${idNum} not found`))
    .then(data => console.log(data))
}
//showOne('http://127.0.0.1:8002/api/pets/', '2');
function destroy(url, idNum) {
    fetch(url + idNum, {
        method: 'DELETE',
        headers: {
            'Content-Type':'application/json'
        }
    })
    .then(response => response.json())
    .then(data => console.log(data))
}
//destroy('http://127.0.0.1:8002/api/pets/', '2');
const petDataDict = {
    nombre_mascota: "aaaake44eent",
    raza: "fr4ecdnch",
    edad: 441,
    color: "yel4xffsdlow",
    telefono: "30444509987640",
    ciudad: "cagf444gfli"
}
function saveOne(url, newPetDataDict ) {
    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type':'application/json'
        },
        body : JSON.stringify(newPetDataDict)
    }).then(res => res)
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', '1'));
}
//saveOne('http://127.0.0.1:8002/api/pets', petDataDict);
//showAll('http://127.0.0.1:8002/api/pets');
const dictWithUpdate = {
    nombre_mascota: "updated3",
    edad: 4413333,
    color: "yel4xffsdl33333ow"
};
function update(url, idNum, dictWithUpdateData) {
    fetch(url + idNum, {
        method: 'PUT',
        headers: {
            'Content-Type':'application/json'
        },
        body : JSON.stringify(dictWithUpdateData)
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response));
}
update('http://127.0.0.1:8002/api/pets/', '1', dictWithUpdate);