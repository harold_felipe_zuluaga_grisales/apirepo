<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    use HasFactory;
    protected $fillable =[
         'nombre_mascota',
         'raza',
         'edad',
         'color',
         'telefono',
         'ciudad'
    ];
}
